package com.comp.master.view;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SiteController {

	@RequestMapping(value = "/")
	public ModelAndView mainPage() {
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/contactus")
	public ModelAndView contactusPage(ModelMap map, HttpServletRequest request) {
		
		long time = request.getSession().getCreationTime();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		Date date = cal.getTime();
		
		map.addAttribute("sessionTime", date);
		return new ModelAndView("contactus");
	}
}
